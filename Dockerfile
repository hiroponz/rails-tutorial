FROM ruby:2.4.7-slim

RUN apt-get update \
 && apt-get -y upgrade \
 && apt-get install -y build-essential patch ruby-dev zlib1g-dev liblzma-dev libpq-dev curl imagemagick \
 && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
 && apt-get install -y nodejs

ARG rails_env=production

ENV RAILS_ENV=${rails_env}

EXPOSE 5000

WORKDIR /app

COPY Gemfile* /app/

RUN bundle install --jobs $(nproc)

COPY . /app/

CMD ["/app/entrypoint.sh"]
